<?php
/**
 * @file
 * Provides generic exception class.
 */

/**
 * A generic exception class.
 */
class TableSizeException extends Exception {}